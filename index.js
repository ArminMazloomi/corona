require('dotenv').config();
const express = require('express');
const configuration = require('./settings');
const gis = require('./gis.js');
const bodyParser = require('body-parser');

const port = process.env.PORT || 3000;

var app = express();

app.use(bodyParser.urlencoded({ extended : true}))

app.use(bodyParser.json())

app.use('/gis', gis);

app.use(function(req, res) {
	res.status(404);
	res.send('Not found');
})

app.listen(port, '0.0.0.0', function(){
	configuration.configure();
});
