const file = require('./file')
const logger = require('./logger')

module.exports = {
	featureCollection : null,
	configure : function() {
		logger.info('configuring server')
		this.featureCollection = file.readFeatureCollection();
		logger.info('configuring server exited with code 0')

	}
};


