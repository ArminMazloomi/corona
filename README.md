# Vision
In age of Covid-19 We want to write an application to help people in this situation. The application purpose is to report if someone is diagnosed with Covid-19 and it collects the information to show how many users are detected in specific area. 
# General Description 
In this application we have to run a webserver in NodeJS that performs some GIS functionality. We have to load the file in memory once the server is booting. Then each request that comes to server we have to process the data in return appropriate response. 

In server initial input file you will be receiving number of polygons with it's GPS coordinates (lat, long). The basic functionality is to be given a GPS coordinate and tell which polygons the point is inside them.  
## APIs 
We have to write number of HTTP API endpoints that can be called by any Http client. 

* A GET (/gis/testpoint) endpoint that receives a pair of parameters (lat, long) and returns a JSON structure which has a member called polygons and it contains the name of the polygons that the point is inside them 
* A PUT (/gis/addpolygon) endpoint that we can add a new polygon to server for subsequent get calls.