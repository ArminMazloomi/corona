const fs = require('fs');
const logger = require('./logger')
const featureCollectionDir = './featureCollection.json'

const readFeatureCollection = function () {
	logger.info('reading from featureCollection file')
	try {
		let featureCollection = JSON.parse(fs.readFileSync(featureCollectionDir));
		logger.info('reading from featureCollection file exited with code 0');
		return featureCollection;
	} catch (err) {
		logger.info('reading from featureCollection file exited with code 1')
	}
}

const writeToFeatureCollection = function (newData) {
	    try {
	    	logger.info('writing to featureCollection file')
	        fs.writeFile(featureCollectionDir, JSON.stringify(newData , null , '\t') , 'utf8', 
	            function(err){
	                if (err) {
	                    logMyErrors(err);
	                    return false;
	                }
	            }
        );
        logger.info('writing to featureCollection file exited with code 0')
        return true;
      } catch (err) {
        logger.info('error' , 
        `an error was occurred while write to file:    ${err}`);
      } finally {
      	logger.info('writing to featureCollection file exited with code 1 ')
          return false;
      }
}

module.exports = {readFeatureCollection, writeToFeatureCollection};
