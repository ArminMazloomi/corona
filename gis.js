const express = require('express');
const configuration = require('./settings');
const turf = require('@turf/turf');
const GJV = require('geojson-validation');
const file = require('./file');
const logger = require('./logger');
const {check, validationResult} = require('express-validator');


var router = express.Router();

router.put('/addpolygon', function(req, res, next){
	let is_valid = false;
	poly = req.body;
	logger.info('validating addpolygon request')
	is_valid = GJV.isPolygon(poly.geometry) && GJV.valid(poly);

	if (is_valid) {
		logger.info('validation exited with code 0');
		next()
	}
	else {
		logger.info('validation exited with code 1');
		res.status(400);
		res.send('Bad request')
	}

});

router.get('/testpoint', [check('long').isFloat(), check('lat').isFloat()] , function(req, res, next){
	logger.info('validating testpoint request')
	const errors = validationResult(req);
	if (!errors.isEmpty()){
		logger.info('validation exited with code 1');
		res.status(400);
		res.send('Bad request')
	} else {
		logger.info('validation exited with code 0')
		next()
	}
});


router.put('/addpolygon', function(req, res){
	configuration.featureCollection.features.push(req.body);
	logger.info('adding polygon to data');
	res.setHeader('Content-Type', 'text/plain');
	res.status(200);
	res.json(configuration.featureCollection);
	file.writeToFeatureCollection(configuration.featureCollection);
	logger.info('adding polygon exited with code 0')
});



router.get('/testpoint', function(req, res){
	point = turf.point([req.query.lat, req.query.long]);
	var filtered = configuration.featureCollection.features.filter(feature => turf.inside(point, feature));
	var reduced = filtered.map(feature => feature.properties.name);
	logger.info('found ${reduced.length} names');
	res.json({ polygons : reduced})
});


module.exports = router;